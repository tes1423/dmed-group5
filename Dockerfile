FROM node:current-alpine3.15

WORKDIR /home/node/app
COPY ./src /home/node/app/src/
COPY ./test /home/node/app/test/
COPY app.arc package.json package-lock.json /home/node/app/
RUN npm install
RUN npm audit fix
RUN npm test
EXPOSE 3333
CMD npm start
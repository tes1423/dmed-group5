locals {
  environments = { "default" : 0, "develop" : 1, "qa" : 2, "production" : 3 }
}

resource "aws_subnet" "main" {
  vpc_id     = var.vpc
  cidr_block = [for w, c in local.environments : "10.0.${c}.0/24" if terraform.workspace == w][0]
}

resource "aws_route_table_association" "main" {
  subnet_id      = aws_subnet.main.id
  route_table_id = var.public_rt
}

resource "aws_ecs_cluster" "main" {
  name = "ecs-node-jest-${terraform.workspace}"
}


resource "aws_ecs_task_definition" "node" {
  family = "node-jest"
  container_definitions = jsonencode([
    {
      name  = "nodejs-jest"
      image = "${var.ecr_uri}:${terraform.workspace}"
      portMappings = [
        {
          containerPort = 3333
        }
      ]
    }
  ])
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = 256
  memory                   = 512
  execution_role_arn       = var.ecs_execution_role
}

resource "aws_ecs_service" "node" {
  name            = "nodejs-jest-${terraform.workspace}"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.node.arn
  desired_count   = 1
  launch_type     = "FARGATE"

  network_configuration {
    subnets          = [aws_subnet.main.id]
    security_groups  = [var.security_group]
    assign_public_ip = true
  }
}
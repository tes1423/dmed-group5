variable "vpc" {
  type = string
}

variable "public_rt" {
  type = string
}

variable "security_group" {
  type = string
}

variable "ecr_uri" {
  type = string
}

variable "ecs_execution_role" {
  type = string
}
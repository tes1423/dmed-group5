terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
  backend "s3" {
    bucket         = "dmed5-gitlab-us-east-1"
    key            = "dmed5-gitlab"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "dmed5-gitlab-us-east-1"
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
  default_tags {
    tags = {
      Owner     = "juandavid.guzman@globant.com"
      Project   = "dmed5-gitlab"
      ManagedBy = "Terraform"
    }
  }
}

